const routes = require('express').Router();
import binanceRoutes from './binance';

routes.use('/binance', binanceRoutes);

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});

module.exports = routes;