const routes = require('express').Router();
import ExchangeFactory from '../../models/binanceFactory';


routes.get('/symbollookup/', async (req, res) => {

    var dataset = await ExchangeFactory().exchangeInfo();
    var data = [];
    if(dataset && dataset.symbols){
        data = dataset.symbols.map(item => {
            return item.symbol;
        });
    };
    res.status(200).json(data);
});

routes.get('/exchangeinfo/', async (req, res) => {
    res.status(200).json(await ExchangeFactory().exchangeInfo());
});

routes.get('/accountinfo/', async (req, res) => {
    res.status(200).json(await ExchangeFactory().accountInfo());
});

routes.get('/orders/open/:symbol/', async (req, res) => {
    res.status(200).json(await ExchangeFactory().openOrders({
        symbol : req.params.symbol,
        timestamp : new Date().getTime()
    }));
});

routes.get('/prices/:symbol/', async (req, res) => {
    res.status(200).json(await ExchangeFactory().prices({
        symbol : req.params.symbol
    }));
});

routes.get('/book/:symbol/', async (req, res) => {
    res.status(200).json(await ExchangeFactory().bookTicker({
        symbol : req.params.symbol
    }));
});



routes.get('/candles/:symbol/:interval/', async (req, res) => {
    
    var data = await ExchangeFactory().candles({
        symbol : req.params.symbol,
        interval : req.params.interval,
        limit : 1000
    });
    
    res.status(200).json(data);
});
  
module.exports = routes;
