import {Decimal} from 'decimal.js';

/*
    Volatility Adjusted Position Units
*/
module.exports = function(accountValue, atr, dollarsPerPoint){

    var av = new Decimal(av);
    var n = new Decimal(atr);
    var dp = new Decimal(dollarsPerPoint);

    return av.times(.01).dividedBy(n.times(dp));
}