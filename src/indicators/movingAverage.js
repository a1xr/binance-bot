import regression from 'regression';
import {Decimal} from 'decimal.js';

class MovingAverage {
    constructor(dataset, period, rgSlopeOptions = {
        order: 2,
        precision: 10,
      }){
        this.dataset = dataset;
        this.period = period;
        this.options = rgSlopeOptions;
    }

    sumDataPoints(dataPointsArray){
        var sum = 0;
        return dataPointsArray.reduce()
    }

    getMaDataSet(){

        if(!this._maDataSet){
            var dataset = this.dataset;
            var maDataSet = [];
            var length = dataset.length;
            var p = this.period - 1; // zero index

            for(var i = p; i < length; i++){
                var start = i - p;
                var slice = dataset.slice(start, start + this.period);
                    maDataSet.push(slice.reduce( (val, cur) => val + cur)/this.period);
            }

            this._maDataSet = maDataSet;
        }
        return this._maDataSet;
    }

    getRegressionLine(){
        if(!this.regLine){
            this.regLine = regression.linear(this.getMaDataSet(), this.options);
        }
        return this.regLine;
    }

    isBelow(x,y){
        var equation = this.getRegressionLine().equation;
        return y <= equation[0] * x + equation[1];
    }

    matchPrecision(x){
        return this.model.flattenXPrecision(x);
    }

    isAbove(x,y){
        var equation = this.getRegressionLine().equation;
        return y > equation[0] * x + equation[1];
    }

    getRegressionSlope(){
        return this.getRegressionLine().equation[0];
    }
}

module.exports = MovingAverage;
