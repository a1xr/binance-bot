import indicator from './indicator'
import binanceModel from '../linearRegressionChannel/binanceModel'

module.exports = {
    ModifiedLinearRegressionChannelIndicator : indicator,
    LinearRegressionChannelModel : binanceModel
};