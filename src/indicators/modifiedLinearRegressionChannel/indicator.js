import LinearRegressionChannelIndicator from '../linearRegressionChannel';

class ModifiedLinearRegressionChannelIndicator extends LinearRegressionChannelIndicator {
    constructor(model, options = {
        order: 2,
        precision: 5,
      }){
        super(model, options);

        this.ANALYSIS_TYPES = {
            SPIKE : 'SPIKE',
            NAIL : 'NAIL',
            UP : 'UP',
            DOWN : 'DOWN'
        };
    }

    getTailLine(){
        if(!this.tailLine){
            var data = super.model.data.close.slice(0, Math.floor(super.model.data.close.length/2));
            this.tailLine = regression.linear(data, this.options);
        }
        return this.tailLine;
    }

    getHeadLine(){
        if(!this.headLine){
            var data = super.model.data.close.slice(Math.floor(super.model.data.close.length/2), super.model.data.close.length);
            this.headLine = regression.linear(data, this.options);
        }
        return this.headLine;
    }

    getTrendAnalysis(){
        var tlSlope = this.getTailLine().equation[0];
        var hlSlope = this.getHeadLine().equation[0];

        if(tlSlope > 0 && hlSlope > 0){
            return this.ANALYSIS_TYPES.UP;
        }else if(tlSlope < 0 && hlSlope < 0){
            return this.ANALYSIS_TYPES.DOWN;
        }else if(tlSlope > 0 && hlSlope < 0){
            return this.ANALYSIS_TYPES.SPIKE;
        }else{
            return this.ANALYSIS_TYPES.NAIL;
        }
    }
}

module.exports = ModifiedLinearRegressionChannelIndicator;
