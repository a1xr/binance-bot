import {Decimal} from 'decimal.js';

var TrueRange = function(high, low, previousClose){
    var h = new Decimal(high);
    var l = new Decimal(low);
    var pc = new Decimal(previousClose);
    return Decimal.max(h.minus(l), Decimal.abs(h-pc), Decimal.abs(pc-l)).toNumber();
};

var AverageTrueRange = function(points, period){
    var sum = 0;

    for(var i = 0; i < points.length; i++){
        var dp = points[i];
        sum += TrueRange(dp.high, dp.low, dp.prevClose);
    }
    return new Decimal(sum).dividedBy(points.length).toNumber();
}

module.exports = {
    TrueRange : TrueRange,
    AverageTrueRange : AverageTrueRange
}