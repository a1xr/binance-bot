import TimeseriesModel from '../models/binanceTimeseriesModel';
import MovingAverage from './movingAverage';
import LinearRegressionChannel from './linearRegressionChannel';
import ModifiedLinearRegressionChannel from './modifiedLinearRegressionChannel/indicator';

//interval  1m 3m 5m 15m 30m 1h 2h 4h 6h 8h 12h 1d 3d 1w 1M

class BinanceIndicatorFactory{
    constructor(exchange){
        this.exchange = exchange;
    }

    getMovingAverage(symbol, interval, period, numPoints, model){
        var model = model ? model : new TimeseriesModel(this.exchange, symbol, interval, numPoints + period - 1).init();
        return async function(){
            model = await model;
            model.load();
            return new MovingAverage(model, period);
        };
    }

    getLinearRegressionChannel(symbol, interval, numPoints, model){
        var model = model ? model : new TimeseriesModel(this.exchange, symbol, interval, numPoints).init();
        return async function(){
            model = await model;
            model.load();
            return new LinearRegressionChannel(model);
        };
    }

    getModifiedLinearRegressionChannel(symbol, interval, numPoints){
        var model = model ? model : new TimeseriesModel(this.exchange, symbol, interval, numPoints).init();
        return async function(){
            model = await model;
            model.load();
            new ModifiedLinearRegressionChannel(model);
        };
    }
}

module.exports = BinanceIndicatorFactory;