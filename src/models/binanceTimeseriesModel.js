
import MockDataRecorder from '../helpers/mockDataRecorder';
import UTCDate from '../helpers/utcDate';

class BinanceTimeseriesModel {
    constructor(exchange, symbol, interval, numPoints){
        this.exchange = exchange;
        this.symbol = symbol;
        //1m 3m 5m 15m 30m 1h 2h 4h 6h 8h 12h 1d 3d 1w 1M
        this.interval = interval;
        this.numPoints = numPoints;
        this._rawdata;
        this.precisionTruncation = 100000;
        this.data = {};
    }

    async init(){

        var curDate = UTCDate.getUTCDate();
        var startDate = UTCDate.getUTCDate();

        if(this.interval.indexOf('m')){
            var intvl = parseInt(this.interval.replace('m', ''));
            startDate.setUTCMinutes(curDate.getUTCMinutes() - intvl*this.numPoints);

        }else if(this.interval.indexOf('h')){
            var intvl = parseInt(this.interval.replace('h', ''));
            startDate.setUTCHours(curDate.getUTCHours() - intvl*this.numPoints);

        }else if(this.interval.indexOf('d')){
            var intvl = parseInt(this.interval.replace('d', ''));
            startDate.setUTCDate(curDate.getUTCDate() - intvl*this.numPoints);
        }

        var endTime = curDate.getTime();
        var startTime = startDate.getTime();

        this._rawdata = await this.exchange.candles({
            symbol : this.symbol,
            interval : this.interval,
            limit : this.numPoints,
            startTime : Number(startTime),
            endTime : Number(endTime)
        });

        if(MockDataRecorder.recordData){
            MockDataRecorder.writeFile('timeseries', [this.symbol, this.interval, this.numPoints].join('-') + '.json', this._rawdata);
        }

        return this;
    }

    load(){
        /*
        openTime: 1560606000000,
        open: '0.00125840',
        high: '0.00125980',
        low: '0.00125780',
        close: '0.00125860',
        volume: '8503677.00000000',
        closeTime: 1560606299999,
        quoteVolume: '10703.16756540',
        trades: 42,
        baseAssetVolume: '7257502.00000000',
        quoteAssetVolume: '9135.18090260'
        */

        var high = [], low=[], close=[], open =[];

        for(var i = 0; i < this._rawdata.length; i++){
            var dp = this._rawdata[i];
            var closeTime = this.flattenXPrecision(dp.closeTime);
            var openTime = this.flattenXPrecision(dp.openTime);

            high.push([closeTime, Number(dp.high)]);
            low.push([closeTime, Number(dp.low)]);
            close.push([closeTime, Number(dp.close)]);
            open.push([openTime, Number(dp.open)]);

        }

        this.data = {
            high : high,
            low : low,
            close : close,
            open : open
        };
    }

    flattenXPrecision(x){
        return Math.round(x/this.precisionTruncation);
    }
}

module.exports = BinanceTimeseriesModel;