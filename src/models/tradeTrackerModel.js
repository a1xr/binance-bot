

class TradeTracker {
    constructor( database, tableName = "trade_tracker" ) {
        this.database = database;
        this.tableName = tableName;

        let createTable = `create table if not exists `+this.tableName+`(
            orderid varchar(255) primary key not null,
            client_orderid varchar(255) not null,
            symbol varchar(255) not null,
            symbol_price decimal(18,8) not null default 0,
            order_fee decimal(18,8) not null default 0,
            lot_size decimal(18,8) not null default 0,
            pip decimal(18,8) not null default 0,
            pipsl decimal(18,8) not null default 0,
            order_type varchar(255) not null,
            ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            transact_time decimal(18,8) default 0,
            raw_response TEXT not null,
            validated INT not null default 0
          )`;
          database.query(createTable, (err, result) => {
            if (err) throw err;
          });
    }

    addTrade(data){
        let sql = 'INSERT INTO ' +this.tableName+ ' SET ?';
        return this.database.query(sql, data);
    };

    removeTrade(data){
      let sql = 'DELETE FROM ' +this.tableName+ ' WHERE orderid = ?';
      return this.database.query(sql, data);
  };
}

module.exports = TradeTracker
