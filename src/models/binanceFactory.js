import Binance from '../binance-api-node';
import BinanceMock from './binanceMock';

module.exports = function(mock = false){
    if(mock){
        return BinanceMock;
    }else{
        return Binance({
            apiKey: process.env.BINANCE_API_KEY,
            apiSecret: process.env.BINANCE_SECRET
        });
    }
};