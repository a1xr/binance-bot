'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.candleFields = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _lodash = require('lodash.zipobject');

var _lodash2 = _interopRequireDefault(_lodash);

require('isomorphic-fetch');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BASE = 'https://api.binance.com';

var defaultGetTime = function defaultGetTime() {
  return Date.now();
};

/**
 * Build query string for uri encoded url based on json object
 */
var makeQueryString = function makeQueryString(q) {
  return q ? '?' + Object.keys(q).map(function (k) {
    return encodeURIComponent(k) + '=' + encodeURIComponent(q[k]);
  }).join('&') : '';
};

/**
 * Finalize API response
 */
var sendResult = function sendResult(call) {
  return call.then(function (res) {
    // If response is ok, we can safely asume it is valid JSON
    if (res.ok) {
      return res.json();
    }

    // Errors might come from the API itself or the proxy Binance is using.
    // For API errors the response will be valid JSON,but for proxy errors
    // it will be HTML
    return res.text().then(function (text) {
      var error = void 0;
      try {
        var json = JSON.parse(text);
        // The body was JSON parseable, assume it is an API response error
        error = new Error(json.msg || res.status + ' ' + res.statusText);
        error.code = json.code;
      } catch (e) {
        // The body was not JSON parseable, assume it is proxy error
        error = new Error(res.status + ' ' + res.statusText + ' ' + text);
        error.response = res;
        error.responseText = text;
      }
      throw error;
    });
  });
};

/**
 * Util to validate existence of required parameter(s)
 */
var checkParams = function checkParams(name, payload) {
  var requires = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  if (!payload) {
    throw new Error('You need to pass a payload object.');
  }

  requires.forEach(function (r) {
    if (!payload[r] && isNaN(payload[r])) {
      throw new Error('Method ' + name + ' requires ' + r + ' parameter.');
    }
  });

  return true;
};

/**
 * Make public calls against the api
 *
 * @param {string} path Endpoint path
 * @param {object} data The payload to be sent
 * @param {string} method HTTB VERB, GET by default
 * @param {object} headers
 * @returns {object} The api response
 */
var publicCall = function publicCall(_ref) {
  var base = _ref.base;
  return function (path, data) {
    var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'GET';
    var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
    return sendResult(fetch(base + '/api' + path + makeQueryString(data), {
      method: method,
      json: true,
      headers: headers
    }));
  };
};

/**
 * Factory method for partial private calls against the api
 *
 * @param {string} path Endpoint path
 * @param {object} data The payload to be sent
 * @param {string} method HTTB VERB, GET by default
 * @returns {object} The api response
 */
var keyCall = function keyCall(_ref2) {
  var apiKey = _ref2.apiKey,
      pubCall = _ref2.pubCall;
  return function (path, data) {
    var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'GET';

    if (!apiKey) {
      throw new Error('You need to pass an API key to make this call.');
    }

    return pubCall(path, data, method, {
      'X-MBX-APIKEY': apiKey
    });
  };
};

/**
 * Factory method for private calls against the api
 *
 * @param {string} path Endpoint path
 * @param {object} data The payload to be sent
 * @param {string} method HTTB VERB, GET by default
 * @param {object} headers
 * @returns {object} The api response
 */
var privateCall = function privateCall(_ref3) {
  var apiKey = _ref3.apiKey,
      apiSecret = _ref3.apiSecret,
      base = _ref3.base,
      _ref3$getTime = _ref3.getTime,
      getTime = _ref3$getTime === undefined ? defaultGetTime : _ref3$getTime,
      pubCall = _ref3.pubCall;
  return function (path) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'GET';
    var noData = arguments[3];
    var noExtra = arguments[4];

    if (!apiKey || !apiSecret) {
      throw new Error('You need to pass an API key and secret to make authenticated calls.');
    }

    return (data && data.useServerTime ? pubCall('/v1/time').then(function (r) {
      return r.serverTime;
    }) : Promise.resolve(getTime())).then(function (timestamp) {
      if (data) {
        delete data.useServerTime;
      }

      var signature = _crypto2.default.createHmac('sha256', apiSecret).update(makeQueryString(_extends({}, data, { timestamp: timestamp })).substr(1)).digest('hex');

      var newData = noExtra ? data : _extends({}, data, { timestamp: timestamp, signature: signature });

      return sendResult(fetch('' + base + (path.includes('/wapi') ? '' : '/api') + path + (noData ? '' : makeQueryString(newData)), {
        method: method,
        headers: { 'X-MBX-APIKEY': apiKey },
        json: true
      }));
    });
  };
};

var candleFields = exports.candleFields = ['openTime', 'open', 'high', 'low', 'close', 'volume', 'closeTime', 'quoteVolume', 'trades', 'baseAssetVolume', 'quoteAssetVolume'];

/**
 * Get candles for a specific pair and interval and convert response
 * to a user friendly collection.
 */
var _candles = function _candles(pubCall, payload) {
  return checkParams('candles', payload, ['symbol']) && pubCall('/v1/klines', _extends({ interval: '5m' }, payload)).then(function (candles) {
    return candles.map(function (candle) {
      return (0, _lodash2.default)(candleFields, candle);
    });
  });
};

/**
 * Create a new order wrapper for market order simplicity
 */
var _order = function _order(privCall) {
  var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var url = arguments[2];

  var newPayload = ['LIMIT', 'STOP_LOSS_LIMIT', 'TAKE_PROFIT_LIMIT'].includes(payload.type) || !payload.type ? _extends({ timeInForce: 'GTC' }, payload) : payload;

  return checkParams('order', newPayload, ['symbol', 'side', 'quantity']) && privCall(url, _extends({ type: 'LIMIT' }, newPayload), 'POST');
};

/**
 * Zip asks and bids reponse from order book
 */
var _book = function _book(pubCall, payload) {
  return checkParams('book', payload, ['symbol']) && pubCall('/v1/depth', payload).then(function (_ref4) {
    var lastUpdateId = _ref4.lastUpdateId,
        asks = _ref4.asks,
        bids = _ref4.bids;
    return {
      lastUpdateId: lastUpdateId,
      asks: asks.map(function (a) {
        return (0, _lodash2.default)(['price', 'quantity'], a);
      }),
      bids: bids.map(function (b) {
        return (0, _lodash2.default)(['price', 'quantity'], b);
      })
    };
  });
};

var _aggTrades = function _aggTrades(pubCall, payload) {
  return checkParams('aggTrades', payload, ['symbol']) && pubCall('/v1/aggTrades', payload).then(function (trades) {
    return trades.map(function (trade) {
      return {
        aggId: trade.a,
        price: trade.p,
        quantity: trade.q,
        firstId: trade.f,
        lastId: trade.l,
        timestamp: trade.T,
        isBuyerMaker: trade.m,
        wasBestPrice: trade.M
      };
    });
  });
};

exports.default = function (opts) {
  var base = opts && opts.httpBase || BASE;
  var pubCall = publicCall(_extends({}, opts, { base: base }));
  var privCall = privateCall(_extends({}, opts, { base: base, pubCall: pubCall }));
  var kCall = keyCall(_extends({}, opts, { pubCall: pubCall }));

  return {
    ping: function ping() {
      return pubCall('/v1/ping').then(function () {
        return true;
      });
    },
    time: function time() {
      return pubCall('/v1/time').then(function (r) {
        return r.serverTime;
      });
    },
    exchangeInfo: function exchangeInfo() {
      return pubCall('/v1/exchangeInfo');
    },

    book: function book(payload) {
      return _book(pubCall, payload);
    },
    aggTrades: function aggTrades(payload) {
      return _aggTrades(pubCall, payload);
    },
    candles: function candles(payload) {
      return _candles(pubCall, payload);
    },

    trades: function trades(payload) {
      return checkParams('trades', payload, ['symbol']) && pubCall('/v1/trades', payload);
    },
    tradesHistory: function tradesHistory(payload) {
      return checkParams('tradesHitory', payload, ['symbol']) && kCall('/v1/historicalTrades', payload);
    },

    dailyStats: function dailyStats(payload) {
      return pubCall('/v1/ticker/24hr', payload);
    },
    prices: function prices(payload){
      return pubCall('/v3/ticker/price', payload);
    },

    avgPrice: function avgPrice(payload) {
      return pubCall('/v3/avgPrice', payload);
    },

    allBookTickers: function allBookTickers() {
      return pubCall('/v1/ticker/allBookTickers').then(function (r) {
        return r.reduce(function (out, cur) {
          return out[cur.symbol] = cur, out;
        }, {});
      });
    },

    bookTicker: function bookTicker(payload){
      return pubCall('/v3/ticker/bookTicker', payload);
    },

    order: function order(payload) {
      return _order(privCall, payload, '/v3/order');
    },
    orderTest: function orderTest(payload) {
      return _order(privCall, payload, '/v3/order/test');
    },
    getOrder: function getOrder(payload) {
      return privCall('/v3/order', payload);
    },
    cancelOrder: function cancelOrder(payload) {
      return privCall('/v3/order', payload, 'DELETE');
    },

    openOrders: function openOrders(payload) {
      return privCall('/v3/openOrders', payload);
    },
    allOrders: function allOrders(payload) {
      return privCall('/v3/allOrders', payload);
    },

    accountInfo: function accountInfo(payload) {
      return privCall('/v3/account', payload);
    },
    myTrades: function myTrades(payload) {
      return privCall('/v3/myTrades', payload);
    },

    withdraw: function withdraw(payload) {
      return privCall('/wapi/v3/withdraw.html', payload, 'POST');
    },
    withdrawHistory: function withdrawHistory(payload) {
      return privCall('/wapi/v3/withdrawHistory.html', payload);
    },
    depositHistory: function depositHistory(payload) {
      return privCall('/wapi/v3/depositHistory.html', payload);
    },
    depositAddress: function depositAddress(payload) {
      return privCall('/wapi/v3/depositAddress.html', payload);
    },
    tradeFee: function tradeFee(payload) {
      return privCall('/wapi/v3/tradeFee.html', payload).then(function (res) {
        return res.tradeFee;
      });
    },
    assetDetail: function assetDetail(payload) {
      return privCall('/wapi/v3/assetDetail.html', payload);
    },

    getDataStream: function getDataStream() {
      return privCall('/v1/userDataStream', null, 'POST', true);
    },
    keepDataStream: function keepDataStream(payload) {
      return privCall('/v1/userDataStream', payload, 'PUT', false, true);
    },
    closeDataStream: function closeDataStream(payload) {
      return privCall('/v1/userDataStream', payload, 'DELETE', false, true);
    }
  };
};