
module.exports = {
    LIMIT : "LIMIT",
    STOP_LOSS : "STOP_LOSS",
    STOP_LOSS_LIMIT : "STOP_LOSS_LIMIT",
    SIDE_SELL : "SELL",
    SIDE_BUY : "BUY"
};

