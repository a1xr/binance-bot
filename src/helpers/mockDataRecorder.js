var fs = require("fs");

const writeFile = function(dir, filename, data){
  const dataDir = appRoot.replace('dist', 'var/mock');
  dir = dir + '_' + getDateString();
  var path = dataDir + '/' + dir + '/';
  writeDirectory(path);

  fs.writeFile(path + filename, JSON.stringify(data), (err) => {
    if (err) console.log(err);
    console.log(filename + " Successfully Written to File.");
  });
}

const writeDirectory = function(targetDir){
  if (!fs.existsSync(targetDir)){
    fs.mkdirSync(targetDir, { recursive: true });
  }
};

const getDateString = function(){
  var d = new Date();

  return d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " +
d.getHours() + ":" + d.getMinutes();
}


module.exports = {
  writeFile : writeFile,
  recordData : false
};

