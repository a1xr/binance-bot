import {Decimal} from 'decimal.js';
import OrderEnums from './orderEnums';
import UUID from 'uuid/v4';
import PipCalculator from './pipCalculator';

class OrderHelper{

    /*
    *   exchange, logger, orderIdPrefix, runlive
    */
    constructor(model){
        this.model = model;
    }

    generateOrderId(){
        var id = UUID();

        return this.model.orderIdPrefix + '-' +  id.substring(this.model.orderIdPrefix.length + 1);
    }

    generateStopLossOrderId(){
        var id = UUID();

        return 'sl-' +  id.substring(3);
    }

    hasOpenOrder(openOrders){
        return openOrders.find(order =>{
            return order.clientOrderId.indexOf(this.model.orderIdPrefix + '-') >= 0;
        }) != undefined;
    }

    hasStaleBuyOrder(openOrders, serverTime, minuteThreshold){
        var orders = openOrders.filter(order =>{
            return order.clientOrderId.indexOf(this.model.orderIdPrefix) >= 0 && order.side == OrderEnums.SIDE_BUY;
        });

        if(orders 
            && orders.length
            && (new Date(serverTime).getUTCMinutes() - new Date(orders[0].time).getUTCMinutes() > minuteThreshold)
        ){
            return orders[0];
        }
        return null;
    }

    /*
        args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
    */
    placeSellOrder(args){
        var symbol = args.symbol;
        var priceModifierPct = args.priceModifierPct || 0;
        
        var tradeFee = new Decimal(args.tradeFee);

        var currentAskPrice = new Decimal(args.currentPrice)
            .plus(new Decimal(args.currentPrice).times(tradeFee))
            .plus(new Decimal(args.currentPrice).times(priceModifierPct));
        
        var lotSize = new Decimal(Number(args.qty).toFixed(args.qtyPrecision));

        var orderRevenue = currentAskPrice.times(lotSize);
        var orderFee = orderRevenue.times(tradeFee);

        var orderArgs = {
            symbol : symbol,
            side : OrderEnums.SIDE_SELL,
            type : OrderEnums.LIMIT,
            quantity : lotSize,
            price : currentAskPrice.toFixed(args.pricePrecision),
            newClientOrderId : this.generateOrderId()
        };

        var orderNote = symbol + " sell price " + currentAskPrice + " qty " + lotSize;
        var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);

        console.log(orderNote);
        return order.then(result =>{
            if(result.clientOrderId){
                this.model.tradeTracker.addTrade({
                    orderid : result.orderId,
                    client_orderid : result.clientOrderId,
                    symbol : result.symbol,
                    transact_time : result.transact_time,
                    symbol_price : result.price,
                    order_fee : orderFee,
                    lot_size : result.origQty,
                    order_type : OrderEnums.SIDE_SELL,
                    raw_response : JSON.stringify(result)
                }).then();
            }
        }).catch((err)=>{
            console.log(err);
            this.model.logger.log(
                orderArgs, 
                err, 
                orderNote, 
                this.model.logger.constants.ERROR
            ).then();
        });
   }

   placeImmediateSellOrder(args){
        var symbol = args.symbol;
        var priceModifierPct = args.priceModifierPct || 0;
        
        var tradeFee = new Decimal(args.tradeFee);

        var currentAskPrice = new Decimal(args.currentPrice);
        
        var lotSize = new Decimal(Number(args.qty).toFixed(args.qtyPrecision));

        var orderRevenue = currentAskPrice.times(lotSize);
        var orderFee = orderRevenue.times(tradeFee);

        var orderArgs = {
            symbol : symbol,
            side : OrderEnums.SIDE_SELL,
            type : OrderEnums.LIMIT,
            quantity : lotSize,
            price : currentAskPrice.toFixed(args.pricePrecision),
            newClientOrderId : this.generateStopLossOrderId()
        };

        var orderNote = symbol + " immediate sell price " + currentAskPrice + " qty " + lotSize;
        var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);

        console.log(orderNote);
        return order.then(result =>{
            if(result.clientOrderId){

                this.model.tradeTracker.addTrade({
                    orderid : result.orderId,
                    client_orderid : result.clientOrderId,
                    symbol : result.symbol,
                    transact_time : result.transact_time,
                    symbol_price : result.price,
                    order_fee : orderFee,
                    lot_size : result.origQty,
                    order_type : OrderEnums.SIDE_SELL,
                    raw_response : JSON.stringify(result)
                }).then();
            }
        }).catch((err)=>{
            this.model.logger.log(
                orderArgs, 
                err, 
                orderNote, 
                this.model.logger.constants.ERROR
            ).then();
        });
    }

    /*
        args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
    */
    placeBuyOrder(args){

        var symbol = args.symbol;
        var priceModifierPct = args.priceModifierPct || 0;

        var orderValue = new Decimal(args.orderValue);
        var tradeFee = new Decimal(args.tradeFee);

        var currentBidPrice = new Decimal(args.currentPrice)
            .minus(new Decimal(args.currentPrice).times(tradeFee))
            .minus(new Decimal(args.currentPrice).times(priceModifierPct));

        var stopLoss = currentBidPrice.minus(new Decimal(args.stopLossPct).times(currentBidPrice));
        var lotSize = orderValue.dividedBy(currentBidPrice).toFixed(args.qtyPrecision);

        var orderFee = orderValue.times(tradeFee);

        //place buy based on risk
        var pip = PipCalculator.getUsdPipRiskReward(orderValue, lotSize, currentBidPrice.decimalPlaces());
        var pipsl = PipCalculator.getUsdPipRiskRewardWithStopLoss(stopLoss, orderValue, lotSize, currentBidPrice.decimalPlaces());

        var price = currentBidPrice.toFixed(args.pricePrecision);

        var orderArgs = {
            symbol : symbol,
            side : OrderEnums.SIDE_BUY,
            type : OrderEnums.LIMIT,
            quantity : lotSize,
            price : price,
            newClientOrderId : this.generateOrderId()
        };

        var orderNote = symbol + " buy price " + price + " qty " + lotSize;
        var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);

        console.log(orderNote);

        return order.then(result =>{
            if(result.clientOrderId){
                this.model.tradeTracker.addTrade({
                    orderid : result.orderId,
                    client_orderid : result.clientOrderId,
                    symbol : result.symbol,
                    transact_time : result.transact_time,
                    symbol_price : result.price,
                    order_fee : orderFee,
                    lot_size : result.origQty,
                    pip : pip,
                    pipsl :  pipsl,
                    order_type : OrderEnums.SIDE_BUY,
                    raw_response : JSON.stringify(result)
                }).then();
            }
        }).catch((err)=>{
            console.log(err);
            this.model.logger.log(
                orderArgs, 
                err, 
                orderNote, 
                this.model.logger.constants.ERROR
            ).then();
        });
    }

    /*
        args: symbol, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee
    */
    setStopLoss(args){

        var currentPrice = new Decimal(args.price);
        var stopLossPrice = currentPrice.minus(new Decimal(args.stopLossPct).times(currentPrice));
        var qty = new Decimal(args.qty).toFixed(assetData.seedData.qtyPrecision);

        var tradeFee = new Decimal(args.tradeFee);
        var orderValue = currentPrice.times(qty);
        var orderFee = orderValue.times(tradeFee);



        var orderArgs = {
            symbol : symbol,
            type : OrderEnums.STOP_LOSS,
            quantity : qty,
            price : stopLossPrice.toFixed(args.pricePrecision),
            newClientOrderId : this.generateStopLossOrderId()
        };

        var orderNote = symbol + " stoploss price " + stopLossPrice + " qty " + qty;
        //console.log(this.model.runlive);
        var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);

        return order.then(result =>{
            this.model.tradeTracker.addTrade({
                orderid : result.orderId,
                client_orderid : result.clientOrderId,
                symbol : result.symbol,
                transact_time : result.transact_time,
                symbol_price : result.price,
                order_fee : orderFee,
                lot_size : result.origQty,
                order_type : OrderEnums.STOP_LOSS,
                raw_response : result.toJSON()
            }).then();
        }).catch((err)=>{
            this.model.logger.log(
                orderArgs, 
                err, 
                orderNote, 
                this.model.logger.constants.ERROR
            ).then();
        });

    }

    cancelOrder(order){

        var orderArgs = {
            symbol : order.symbol,
            orderId : order.orderId
        };

        var orderNote = order.symbol + " cancel_order price " + order.orderId;
        console.log(orderNote);
        return this.model.exchange.cancelOrder(orderArgs).then(result=>{
            this.model.tradeTracker.removeTrade({         
                orderId : result.orderId
            }).then();

            }).catch((err)=>{
                this.model.logger.log(
                    orderArgs, 
                    err, 
                    orderNote, 
                    this.model.logger.constants.ERROR
                ).then();
        });;
    }
}

module.exports = OrderHelper;