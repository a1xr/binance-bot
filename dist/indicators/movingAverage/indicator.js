"use strict";

var _regression = _interopRequireDefault(require("regression"));

var _decimal = require("decimal.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MovingAverage =
/*#__PURE__*/
function () {
  function MovingAverage(model, period) {
    var ohlcType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "close";
    var rgSlopeOptions = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {
      order: 2,
      precision: 10
    };

    _classCallCheck(this, MovingAverage);

    this.model = model;
    this.period = period;
    this.ohlcType = ohlcType;
    this.options = rgSlopeOptions;
  }

  _createClass(MovingAverage, [{
    key: "sumDataPoints",
    value: function sumDataPoints(dataPointsArray) {
      var sum = 0;
      return dataPointsArray.reduce();
    }
  }, {
    key: "getMaDataSet",
    value: function getMaDataSet() {
      if (!this._maDataSet) {
        var dataset = this.model.data[this.ohlcType];
        var maDataSet = [];

        for (var i = 0; i < dataset.length; i += this.period) {
          var slice = dataset.slice(i, i + this.period);
          maDataSet.push(slice.reduce(function (val, cur) {
            return val + cur;
          }));
        }

        this._maDataSet = maDataSet;
      }

      return this._maDataSet;
    }
  }, {
    key: "getRegressionLine",
    value: function getRegressionLine() {
      if (!this.regLine) {
        this.regLine = _regression["default"].linear(this.getMaDataSet(), this.options);
      }

      return this.regLine;
    }
  }, {
    key: "isBelow",
    value: function isBelow(x, y) {
      var equation = this.getRegressionLine().equation;
      return y <= equation[0] * x + equation[1];
    }
  }, {
    key: "matchPrecision",
    value: function matchPrecision(x) {
      return this.model.flattenXPrecision(x);
    }
  }, {
    key: "isAbove",
    value: function isAbove(x, y) {
      var equation = this.getRegressionLine().equation;
      return y > equation[0] * x + equation[1];
    }
  }, {
    key: "getRegressionSlope",
    value: function getRegressionSlope() {
      return this.getRegressionLine().equation[0];
    }
  }]);

  return MovingAverage;
}();

module.exports = MovingAverage;