"use strict";

var _decimal = require("decimal.js");

//NOTE: I do not believe this is accurate.
function getUsdPip(precision) {
  return new _decimal.Decimal(1).dividedBy(new _decimal.Decimal(10).pow(precision));
}

function getUsdPipValue(lotSize, precision) {
  return new _decimal.Decimal(lotSize).times(getUsdPip(precision));
}

function getUsdPipRiskReward(risk, lotSize, precision) {
  return new _decimal.Decimal(risk).times(getUsdPipValue(lotSize, precision));
}

function getUsdPipRiskRewardWithStopLoss(stopLoss, risk, lotSize, precision) {
  return new _decimal.Decimal(risk).dividedBy(stopLoss).times(getUsdPipValue(lotSize, precision));
}

module.exports = {
  getUsdPip: getUsdPip,
  getUsdPipValue: getUsdPipValue,
  getUsdPipRiskReward: getUsdPipRiskReward,
  getUsdPipRiskRewardWithStopLoss: getUsdPipRiskRewardWithStopLoss
};