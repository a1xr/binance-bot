"use strict";

var _binanceTimeseriesModel = _interopRequireDefault(require("../models/binanceTimeseriesModel"));

var _movingAverage = _interopRequireDefault(require("./movingAverage"));

var _linearRegressionChannel = _interopRequireDefault(require("./linearRegressionChannel"));

var _indicator = _interopRequireDefault(require("./modifiedLinearRegressionChannel/indicator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//interval  1m 3m 5m 15m 30m 1h 2h 4h 6h 8h 12h 1d 3d 1w 1M
var BinanceIndicatorFactory =
/*#__PURE__*/
function () {
  function BinanceIndicatorFactory(exchange) {
    _classCallCheck(this, BinanceIndicatorFactory);

    this.exchange = exchange;
  }

  _createClass(BinanceIndicatorFactory, [{
    key: "getMovingAverage",
    value: function getMovingAverage(symbol, interval, period, numPoints, model) {
      var model = model ? model : new _binanceTimeseriesModel["default"](this.exchange, symbol, interval, numPoints + period - 1).init();
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return model;

                case 2:
                  model = _context.sent;
                  model.load();
                  return _context.abrupt("return", new _movingAverage["default"](model, period));

                case 5:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }))
      );
    }
  }, {
    key: "getLinearRegressionChannel",
    value: function getLinearRegressionChannel(symbol, interval, numPoints, model) {
      var model = model ? model : new _binanceTimeseriesModel["default"](this.exchange, symbol, interval, numPoints).init();
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee2() {
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return model;

                case 2:
                  model = _context2.sent;
                  model.load();
                  return _context2.abrupt("return", new _linearRegressionChannel["default"](model));

                case 5:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        }))
      );
    }
  }, {
    key: "getModifiedLinearRegressionChannel",
    value: function getModifiedLinearRegressionChannel(symbol, interval, numPoints) {
      var model = model ? model : new _binanceTimeseriesModel["default"](this.exchange, symbol, interval, numPoints).init();
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee3() {
          return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return model;

                case 2:
                  model = _context3.sent;
                  model.load();
                  new _indicator["default"](model);

                case 5:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }))
      );
    }
  }]);

  return BinanceIndicatorFactory;
}();

module.exports = BinanceIndicatorFactory;