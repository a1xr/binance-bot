"use strict";

var _decimal = require("decimal.js");

var TrueRange = function TrueRange(high, low, previousClose) {
  var h = new _decimal.Decimal(high);
  var l = new _decimal.Decimal(low);
  var pc = new _decimal.Decimal(previousClose);
  return _decimal.Decimal.max(h.minus(l), _decimal.Decimal.abs(h - pc), _decimal.Decimal.abs(pc - l)).toNumber();
};

var AverageTrueRange = function AverageTrueRange(points, period) {
  var sum = 0;

  for (var i = 0; i < points.length; i++) {
    var dp = points[i];
    sum += TrueRange(dp.high, dp.low, dp.prevClose);
  }

  return new _decimal.Decimal(sum).dividedBy(points.length).toNumber();
};

module.exports = {
  TrueRange: TrueRange,
  AverageTrueRange: AverageTrueRange
};