"use strict";

var _binanceApiNode = _interopRequireDefault(require("../binance-api-node"));

var _binanceMock = _interopRequireDefault(require("./binanceMock"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = function () {
  var mock = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

  if (mock) {
    return _binanceMock["default"];
  } else {
    return (0, _binanceApiNode["default"])({
      apiKey: process.env.BINANCE_API_KEY,
      apiSecret: process.env.BINANCE_SECRET
    });
  }
};