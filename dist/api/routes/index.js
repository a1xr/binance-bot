"use strict";

var _binance = _interopRequireDefault(require("./binance"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = require('express').Router();

routes.use('/binance', _binance["default"]);
routes.get('/', function (req, res) {
  res.status(200).json({
    message: 'Connected!'
  });
});
module.exports = routes;