"use strict";

var _express = _interopRequireDefault(require("express"));

var _routes = _interopRequireDefault(require("./routes"));

require("dotenv/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Set up the express app
var app = (0, _express["default"])();
app.use(_routes["default"]); // get all todos

var PORT = process.env.PORT;
app.listen(PORT, function () {
  console.log("server running on port ".concat(PORT));
});