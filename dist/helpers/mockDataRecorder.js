"use strict";

var fs = require("fs");

var writeFile = function writeFile(dir, filename, data) {
  var dataDir = appRoot.replace('dist', 'var/mock');
  dir = dir + '_' + getDateString();
  var path = dataDir + '/' + dir + '/';
  writeDirectory(path);
  fs.writeFile(path + filename, JSON.stringify(data), function (err) {
    if (err) console.log(err);
    console.log(filename + " Successfully Written to File.");
  });
};

var writeDirectory = function writeDirectory(targetDir) {
  if (!fs.existsSync(targetDir)) {
    fs.mkdirSync(targetDir, {
      recursive: true
    });
  }
};

var getDateString = function getDateString() {
  var d = new Date();
  return d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
};

module.exports = {
  writeFile: writeFile,
  recordData: false
};