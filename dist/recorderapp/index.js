"use strict";

require("@babel/polyfill");

var _MockDataRecorder = _interopRequireDefault(require("../helpers/MockDataRecorder"));

var _binanceFactory = _interopRequireDefault(require("../models/binanceFactory"));

var _utcDate = _interopRequireDefault(require("../helpers/utcDate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var path = require('path');

global.appRoot = path.resolve(__dirname);
global.appRoot = appRoot.replace('/recorderapp', '');
var exchange = (0, _binanceFactory["default"])();

var record = function record(symbol) {
  recordData(symbol);
};

var recordData = function recordData(symbol) {
  var limit = 1000;

  var curDate = _utcDate["default"].getUTCDate();

  var startDate = _utcDate["default"].getUTCDate();

  startDate.getUTCDate(-1 * limit);
  var endTime = curDate.getTime();
  var startTime = startDate.getTime();
  var interval = '1d';
  exchange.candles({
    symbol: symbol,
    interval: interval,
    limit: limit
  }).then(function (data) {
    console.log(symbol);

    _MockDataRecorder["default"].writeFile('timeseries', [symbol, interval, endTime].join('-') + '.json', data);
  });
};

var symbols = [];
var i = 0;
var timer;

module.exports = function () {
  exchange.exchangeInfo().then(
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(data) {
      var offset;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              symbols = data.symbols;
              offset = 1000;
              timer = setInterval(function () {
                if (i < symbols.length) {
                  record(symbols[i].symbol);
                  i++;
                } else {
                  clearInterval(timer);
                }
              }, 1000);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }());
};