"use strict";

var _movingAverage = _interopRequireDefault(require("../indicators/movingAverage"));

var _atr = require("../indicators/atr");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var expect = require('chai').expect;

var dataSeries5m11 = require('../../var/mock/timeseries_18-6-2019 22:28/NEOUSDT-5m-11.json');

var dataSeries5m31 = require('../../var/mock/timeseries_18-6-2019 22:28/NEOUSDT-5m-31.json');

var dataSeries5m50 = require('../../var/mock/timeseries_18-6-2019 22:28/NEOUSDT-5m-50.json');

describe('Indicators', function () {
  describe('Moving Average', function () {
    describe('MA : Period 5 w/ 11 datapoints', function () {
      it('status', function (done) {
        var ds = dataSeries5m11.map(function (item) {
          return Number(item.close);
        });
        var period = 5;
        var ma = new _movingAverage["default"](ds, period);
        var maSeries = ma.getMaDataSet();
        expect(maSeries.length).to.equal(ds.length - (period - 1));
        done();
      });
    });
    describe('MA : Period 5 w/ 31 datapoints', function () {
      it('status', function (done) {
        var ds = dataSeries5m31.map(function (item) {
          return Number(item.close);
        });
        var period = 5;
        var ma = new _movingAverage["default"](ds, period);
        var maSeries = ma.getMaDataSet();
        expect(maSeries.length).to.equal(ds.length - (period - 1));
        done();
      });
    });
    describe('MA : Period 5 w/ 50 datapoints', function () {
      it('status', function (done) {
        var ds = dataSeries5m50.map(function (item) {
          return Number(item.close);
        });
        var period = 5;
        var ma = new _movingAverage["default"](ds, period);
        var maSeries = ma.getMaDataSet();
        expect(maSeries.length).to.equal(ds.length - (period - 1));
        done();
      });
    });
  });
  describe('Average True Range', function () {
    describe('TrueRange Simple', function () {
      it('status', function (done) {
        var val = (0, _atr.TrueRange)(2, 1, 1.5);
        expect(val).to.equal(1);
        done();
      });
    });
    describe('TrueRange NEO', function () {
      it('status', function (done) {
        var val = (0, _atr.TrueRange)(13.86700000, 13.84200000, 13.83600000);
        expect(val).to.equal(0.031000000000000583);
        done();
      });
    });
    describe('TrueRange BTC', function () {
      it('status', function (done) {
        //need to update with BTC values
        var val = (0, _atr.TrueRange)(13.86700000, 13.84200000, 13.83600000);
        expect(val).to.equal(0.031000000000000583);
        done();
      });
    });
    describe('ATR NEO', function () {
      it('status', function (done) {
        var ds = [];

        for (var i = 1; i < dataSeries5m11.length; i++) {
          var pc = Number(dataSeries5m11[i - 1].close);
          var high = Number(dataSeries5m11[i].high);
          var low = Number(dataSeries5m11[i].low);
          var dp = {
            prevClose: pc,
            high: high,
            low: low
          }; //console.log(dp);

          ds.push(dp);
        }

        var val = (0, _atr.AverageTrueRange)(ds);
        expect(val).to.equal(0.03160000000000006);
        done();
      });
    });
  });
});