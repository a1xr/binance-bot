"use strict";

var _mockDataRecorder = _interopRequireDefault(require("../../helpers/mockDataRecorder"));

var _utcDate = _interopRequireDefault(require("../../helpers/utcDate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var BinanceTimeseriesModel =
/*#__PURE__*/
function () {
  function BinanceTimeseriesModel(exchange, symbol, interval, numPoints) {
    _classCallCheck(this, BinanceTimeseriesModel);

    this.exchange = exchange;
    this.symbol = symbol; //1m 3m 5m 15m 30m 1h 2h 4h 6h 8h 12h 1d 3d 1w 1M

    this.interval = interval;
    this.numPoints = numPoints;
    this._rawdata;
    this.precisionTruncation = 100000;
    this.data = {};
  }

  _createClass(BinanceTimeseriesModel, [{
    key: "init",
    value: function () {
      var _init = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var curDate, startDate, intvl, endTime, startTime;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                curDate = _utcDate["default"].getUTCDate();
                startDate = _utcDate["default"].getUTCDate();

                if (this.interval.indexOf('m')) {
                  intvl = parseInt(this.interval.replace('m', ''));
                  startDate.setUTCMinutes(curDate.getUTCMinutes() - intvl * this.numPoints);
                } else if (this.interval.indexOf('h')) {
                  intvl = parseInt(this.interval.replace('h', ''));
                  startDate.setUTCHours(curDate.getUTCHours() - intvl * this.numPoints);
                } else if (this.interval.indexOf('d')) {
                  intvl = parseInt(this.interval.replace('d', ''));
                  startDate.setUTCDate(curDate.getUTCDate() - intvl * this.numPoints);
                }

                endTime = curDate.getTime();
                startTime = startDate.getTime();
                _context.next = 7;
                return this.exchange.candles({
                  symbol: this.symbol,
                  interval: this.interval,
                  limit: this.numPoints,
                  startTime: Number(startTime),
                  endTime: Number(endTime)
                });

              case 7:
                this._rawdata = _context.sent;

                if (_mockDataRecorder["default"].recordData) {
                  _mockDataRecorder["default"].writeFile('timeseries', [this.symbol, this.interval, this.numPoints].join('-') + '.json', this._rawdata);
                }

                return _context.abrupt("return", this);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function init() {
        return _init.apply(this, arguments);
      }

      return init;
    }()
  }, {
    key: "load",
    value: function load() {
      /*
      openTime: 1560606000000,
      open: '0.00125840',
      high: '0.00125980',
      low: '0.00125780',
      close: '0.00125860',
      volume: '8503677.00000000',
      closeTime: 1560606299999,
      quoteVolume: '10703.16756540',
      trades: 42,
      baseAssetVolume: '7257502.00000000',
      quoteAssetVolume: '9135.18090260'
      */
      var high = [],
          low = [],
          close = [],
          open = [];

      for (var i = 0; i < this._rawdata.length; i++) {
        var dp = this._rawdata[i];
        var closeTime = this.flattenXPrecision(dp.closeTime);
        var openTime = this.flattenXPrecision(dp.openTime);
        high.push([closeTime, Number(dp.high)]);
        low.push([closeTime, Number(dp.low)]);
        close.push([closeTime, Number(dp.close)]);
        open.push([openTime, Number(dp.open)]);
      }

      this.data = {
        high: high,
        low: low,
        close: close,
        open: open
      };
    }
  }, {
    key: "flattenXPrecision",
    value: function flattenXPrecision(x) {
      return Math.round(x / this.precisionTruncation);
    }
  }]);

  return BinanceTimeseriesModel;
}();

module.exports = BinanceTimeseriesModel;