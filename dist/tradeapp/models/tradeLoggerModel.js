"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TradeLoggerModel =
/*#__PURE__*/
function () {
  function TradeLoggerModel(database) {
    _classCallCheck(this, TradeLoggerModel);

    this.database = database;
    this.tableName = "trade_log";
    this.constants = {
      ERROR: "ERROR",
      SUCCESS: "SUCCESS"
    };
    var createTable = "create table if not exists " + this.tableName + "(\n            id INT AUTO_INCREMENT PRIMARY KEY,\n            quote MEDIUMTEXT not null,\n            trade_response MEDIUMTEXT not null,\n            note varchar(255) not null,\n            entry_type varchar(255) not null,\n            ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP\n          )";
    database.query(createTable, function (err, result) {
      if (err) throw err; //console.log(result);
    });
  }

  _createClass(TradeLoggerModel, [{
    key: "log",
    value: function log(quote, tradeResponse, note) {
      var entryType = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "SUCCESS";
      var sql = 'INSERT INTO ' + this.tableName + ' SET ?';
      return this.database.query(sql, {
        quote: JSON.stringify(quote),
        trade_response: JSON.stringify(tradeResponse),
        note: note,
        entry_type: entryType
      });
    }
  }]);

  return TradeLoggerModel;
}();

module.exports = TradeLoggerModel;