"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TradeTracker =
/*#__PURE__*/
function () {
  function TradeTracker(database) {
    var tableName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "trade_tracker";

    _classCallCheck(this, TradeTracker);

    this.database = database;
    this.tableName = tableName;
    var createTable = "create table if not exists " + this.tableName + "(\n            orderid varchar(255) primary key not null,\n            client_orderid varchar(255) not null,\n            symbol varchar(255) not null,\n            symbol_price decimal(18,8) not null default 0,\n            order_fee decimal(18,8) not null default 0,\n            lot_size decimal(18,8) not null default 0,\n            pip decimal(18,8) not null default 0,\n            pipsl decimal(18,8) not null default 0,\n            order_type varchar(255) not null,\n            ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n            transact_time decimal(18,8) default 0,\n            raw_response TEXT not null,\n            validated INT not null default 0\n          )";
    database.query(createTable, function (err, result) {
      if (err) throw err;
    });
  }

  _createClass(TradeTracker, [{
    key: "addTrade",
    value: function addTrade(data) {
      var sql = 'INSERT INTO ' + this.tableName + ' SET ?';
      return this.database.query(sql, data);
    }
  }, {
    key: "removeTrade",
    value: function removeTrade(data) {
      var sql = 'DELETE FROM ' + this.tableName + ' WHERE orderid = ?';
      return this.database.query(sql, data);
    }
  }]);

  return TradeTracker;
}();

module.exports = TradeTracker;