"use strict";

var _decimal = require("decimal.js");

var _orderHelper = _interopRequireDefault(require("../../../helpers/orderHelper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var LinearRegressionAlgo =
/*#__PURE__*/
function () {
  function LinearRegressionAlgo(model) {
    _classCallCheck(this, LinearRegressionAlgo);

    this.model = model; //exchange, logger, orderIdPrefix, runlive

    this.orderHelper = new _orderHelper["default"]({
      exchange: model.exchange,
      logger: model.logger,
      orderIdPrefix: model.orderIdPrefix,
      runlive: model.runlive
    });
  }

  _createClass(LinearRegressionAlgo, [{
    key: "step",
    value: function step() {
      var data = this.model.data;

      for (var symbol in data) {
        var assetModel = data[symbol];
        var indicator = assetModel.lrcIndicator;
        var quote = assetModel.quote;
        var ohlc = quote.ohlc && quote.ohlc.length ? quote.ohlc[0] : {};

        if (indicator.getChannelSlope() > 0) {
          var closeTime = indicator.matchPrecision(ohlc.closeTime);
          /*
          args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee
          */

          var orderArgs = {
            symbol: symbol,
            qtyPrecision: assetModel.seedData.qtyPrecision,
            currentPrice: assetModel.quote.price.price,
            stopLossPct: this.model.seedData.tradeDetails.stopLossPct,
            tradeFee: this.model.seedData.tradeDetails.tradeFee
          }; //BUY

          if (indicator.isBelowChannel(closeTime, ohlc.low) && indicator.getNumTouchesBottom() > 1 && assetModel.openOrders.length == 0) {
            orderArgs.orderValue = assetModel.seedData.orderValue;
            this.orderActions.placeBuyOrder(orderArgs); //SELL
          } else if (indicator.isAboveChannel(closeTime, ohlc.high) && new _decimal.Decimal(assetModel.accountInfo.free).greaterThan(new _decimal.Decimal(this.model.seedData.tradeDetails.lotSize.minQty))) {
            orderArgs.qty = Math.min(Number(assetModel.accountInfo.free), this.model.seedData.tradeDetails.lotSize.maxQty);
            this.orderActions.placeSellOrder(orderArgs);
          }
        }
      }
    }
  }, {
    key: "_run",
    value: function _run() {
      var _this = this;

      this.model.init().then(function () {
        _this.model.load().then(function () {
          _this.step();
        });
      });
    }
  }, {
    key: "run",
    value: function run() {
      var _this2 = this;

      this._run();

      setInterval(function (args) {
        _this2._run();
      }, this.model.runInterval * 1000);
    }
  }]);

  return LinearRegressionAlgo;
}();

module.exports = LinearRegressionAlgo;