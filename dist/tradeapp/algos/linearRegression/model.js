"use strict";

var _binanceIndicatorFactory = _interopRequireDefault(require("../../../indicators/binanceIndicatorFactory"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var LinearRegressionAlgoModel =
/*#__PURE__*/
function () {
  function LinearRegressionAlgoModel(seedData, exchange, tradeTracker, logger) {
    var limitFundsNum = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 3;
    var runInterval = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 5;

    _classCallCheck(this, LinearRegressionAlgoModel);

    this.seedData = seedData;
    this.exchange = exchange;
    this.tradeTracker = tradeTracker;
    this.logger = logger;
    this.limitFundsNum = limitFundsNum;
    this.runlive = true;
    this.orderIdPrefix = "lrc";
    this.runInterval = runInterval;
    this.data = {};
    this._rawData;
    this.indicatorFactory = new _binanceIndicatorFactory["default"](this.exchange);
  }

  _createClass(LinearRegressionAlgoModel, [{
    key: "init",
    value: function () {
      var _init = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var openOrders, lrcIndicators, shortMaIndicators, longMaIndicators, price, bidAsk, ohlc, symbols, symbol;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                openOrders = {};
                lrcIndicators = {};
                shortMaIndicators = {};
                longMaIndicators = {};
                price = {};
                bidAsk = {};
                ohlc = {};
                symbols = this.seedData.symbols;

                for (symbol in symbols) {
                  openOrders[symbol] = this.exchange.openOrders({
                    symbol: symbol,
                    timestamp: new Date().getTime()
                  });
                  lrcIndicators[symbol] = this.indicatorFactory.getLinearRegressionChannel(symbol, '5m', 50);
                  shortMaIndicators[symbol] = this.indicatorFactory.getMovingAverage(symbol, '5m', 5, 7);
                  longMaIndicators[symbol] = this.indicatorFactory.getMovingAverage(symbol, '5m', 25, 7);
                  price[symbol] = this.exchange.prices({
                    symbol: symbol
                  });
                  bidAsk[symbol] = this.exchange.bookTicker({
                    symbol: symbol
                  });
                  ohlc[symbol] = this.exchange.candles({
                    symbol: symbol,
                    interval: '1m',
                    limit: 1
                  });
                }

                _context.next = 11;
                return Promise.all([this.exchange.exchangeInfo(), this.exchange.accountInfo(), openOrders, lrcIndicators, shortMaIndicators, longMaIndicators, price, bidAsk, ohlc]);

              case 11:
                this._rawData = _context.sent;

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function init() {
        return _init.apply(this, arguments);
      }

      return init;
    }()
  }, {
    key: "load",
    value: function () {
      var _load = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var modeledData, rawData, baseCurrency, baseFunds, symbols, symbol, openOrders, lrcIndicator, shortMaIndicator, longMaIndicator, quote;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                modeledData = {};
                rawData = this._rawData;
                baseCurrency = this.seedData.tradeDetails.baseCurrency;
                baseFunds = rawData[1].balances.find(function (item) {
                  return item.asset == baseCurrency;
                });
                symbols = this.seedData.symbols;
                _context2.t0 = regeneratorRuntime.keys(symbols);

              case 6:
                if ((_context2.t1 = _context2.t0()).done) {
                  _context2.next = 33;
                  break;
                }

                symbol = _context2.t1.value;
                _context2.next = 10;
                return rawData[2][symbol];

              case 10:
                openOrders = _context2.sent;
                _context2.next = 13;
                return rawData[3][symbol]();

              case 13:
                lrcIndicator = _context2.sent;
                _context2.next = 16;
                return rawData[4][symbol]();

              case 16:
                shortMaIndicator = _context2.sent;
                _context2.next = 19;
                return rawData[5][symbol]();

              case 19:
                longMaIndicator = _context2.sent;
                _context2.next = 22;
                return rawData[6][symbol];

              case 22:
                _context2.t2 = _context2.sent;
                _context2.next = 25;
                return rawData[7][symbol];

              case 25:
                _context2.t3 = _context2.sent;
                _context2.next = 28;
                return rawData[8][symbol];

              case 28:
                _context2.t4 = _context2.sent;
                quote = {
                  price: _context2.t2,
                  bidask: _context2.t3,
                  ohlc: _context2.t4
                };
                modeledData[symbol] = {
                  seedData: symbols[symbol],
                  exchangeInfo: rawData[0].symbols.find(function (item) {
                    return item.symbol == symbol;
                  }),
                  accountInfo: rawData[1].balances.find(function (item) {
                    return symbol.indexOf(item.asset) >= 0 && item.asset != baseCurrency && item.asset != "TUSD";
                  }),
                  openOrders: openOrders,
                  lrcIndicator: lrcIndicator,
                  shortMaIndicator: shortMaIndicator,
                  longMaIndicator: longMaIndicator,
                  quote: quote,
                  baseFunds: baseFunds
                };
                _context2.next = 6;
                break;

              case 33:
                this.data = modeledData;

              case 34:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function load() {
        return _load.apply(this, arguments);
      }

      return load;
    }()
  }]);

  return LinearRegressionAlgoModel;
}();

module.exports = LinearRegressionAlgoModel;