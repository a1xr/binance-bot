"use strict";

var _decimal = require("decimal.js");

var _orderHelper = _interopRequireDefault(require("../../../helpers/orderHelper"));

var _utcDate = _interopRequireDefault(require("../../../helpers/utcDate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
*   Basic algorithm to sell high and buy low. Intent is for assets more like BTC that have a stable consistant value or upside.
*   The idea is that BTC has a 4-6% flux during the day.
*/
var HighLowAlgo =
/*#__PURE__*/
function () {
  function HighLowAlgo(model) {
    _classCallCheck(this, HighLowAlgo);

    this.model = model; //exchange, logger, orderIdPrefix, runlive

    this.orderHelper = new _orderHelper["default"]({
      exchange: model.exchange,
      logger: model.logger,
      tradeTracker: model.tradeTracker,
      orderIdPrefix: model.orderIdPrefix,
      runlive: model.runlive
    });
    this._lastPriceSet = _utcDate["default"].getUTCDate(-1); //triggers set in step function

    this._stopLossPrice = 0;
  }

  _createClass(HighLowAlgo, [{
    key: "stopLossSettingTracker",
    value: function stopLossSettingTracker(price, stopLossPct) {
      var curDate = _utcDate["default"].getUTCDate();

      if (this._lastPriceSet.getTime() < curDate.getTime()) {
        var price = new _decimal.Decimal(price);
        var stopLossMod = price.times(new _decimal.Decimal(stopLossPct));
        this._stopLossPrice = price.minus(stopLossMod);
        this._lastPriceSet = _utcDate["default"].getUTCDate(6);
      }
    }
  }, {
    key: "triggerStopLoss",
    value: function triggerStopLoss(price) {
      return new _decimal.Decimal(price).lessThan(this._stopLossPrice);
    }
  }, {
    key: "step",
    value: function step() {
      var data = this.model.data;

      for (var symbol in data) {
        var assetModel = data[symbol];
        /*
            args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
        */

        var minQty = new _decimal.Decimal(this.model.seedData.tradeDetails.lotSize.minQty);
        var freeQty = new _decimal.Decimal(assetModel.accountInfo.free);
        var orderArgs = {
          symbol: symbol,
          qtyPrecision: assetModel.seedData.qtyPrecision,
          pricePrecision: assetModel.seedData.pricePrecision,
          currentPrice: assetModel.quote.price.price,
          stopLossPct: this.model.seedData.tradeDetails.stopLossPct,
          tradeFee: this.model.seedData.tradeDetails.tradeFee,
          priceModifierPct: this.model.seedData.priceModifierPct
        };
        this.stopLossSettingTracker(orderArgs.currentPrice, orderArgs.stopLossPct);
        var hasOpenOrder = this.orderHelper.hasOpenOrder(assetModel.openOrders);
        var staleBuyOrder = this.orderHelper.hasStaleBuyOrder(assetModel.openOrders, assetModel.exchangeInfo.serverTime, 30);

        if (staleBuyOrder) {
          console.log('staleorder');
          this.orderHelper.cancelOrder(staleBuyOrder);
        } else if (this.triggerStopLoss(orderArgs.currentPrice)) {
          this.orderHelper.placeImmediateSellOrder(orderArgs); //BUY
        } else if (!hasOpenOrder && freeQty.lessThanOrEqualTo(minQty * 2)) {
          orderArgs.orderValue = assetModel.seedData.orderValue;
          this.orderHelper.placeBuyOrder(orderArgs); //SELL
        } else if (!hasOpenOrder && freeQty.greaterThan(minQty)) {
          orderArgs.qty = Math.min(freeQty.minus(minQty).toFixed(assetModel.seedData.qtyPrecision), this.model.seedData.tradeDetails.lotSize.maxQty);
          this.orderHelper.placeSellOrder(orderArgs);
        }
      }
    }
  }, {
    key: "_run",
    value: function _run() {
      var _this = this;

      this.model.init().then(function () {
        _this.model.load().then(function () {
          _this.step();
        });
      });
    }
  }, {
    key: "run",
    value: function run() {
      var _this2 = this;

      this._run();

      setInterval(function (args) {
        _this2._run();
      }, this.model.runInterval * 1000);
    }
  }]);

  return HighLowAlgo;
}();

module.exports = HighLowAlgo;