"use strict";

var _mockDataRecorder = _interopRequireDefault(require("../../../helpers/mockDataRecorder"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var HighLowAlgoModel =
/*#__PURE__*/
function () {
  function HighLowAlgoModel(seedData, exchange, tradeTracker, logger) {
    var runInterval = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 60;
    var tradeLabel = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';

    _classCallCheck(this, HighLowAlgoModel);

    this.seedData = seedData;
    this.exchange = exchange;
    this.tradeTracker = tradeTracker;
    this.logger = logger;
    this.runlive = true;
    this.orderIdPrefix = tradeLabel ? "hl_" + tradeLabel : "hl";
    this.runInterval = runInterval;
    this.data = {};
    this._rawData;
  }

  _createClass(HighLowAlgoModel, [{
    key: "init",
    value: function () {
      var _init = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var openOrders, price, bidAsk, ohlc, symbols, symbol;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                openOrders = {};
                price = {};
                bidAsk = {};
                ohlc = {};
                symbols = this.seedData.symbols;

                for (symbol in symbols) {
                  openOrders[symbol] = this.exchange.openOrders({
                    symbol: symbol,
                    timestamp: new Date().getTime()
                  });
                  price[symbol] = this.exchange.prices({
                    symbol: symbol
                  });
                  bidAsk[symbol] = this.exchange.bookTicker({
                    symbol: symbol
                  });
                  ohlc[symbol] = this.exchange.candles({
                    symbol: symbol,
                    interval: '1m',
                    limit: 1
                  });
                }

                _context.next = 8;
                return Promise.all([this.exchange.exchangeInfo(), this.exchange.accountInfo(), openOrders, price, bidAsk, ohlc]);

              case 8:
                this._rawData = _context.sent;

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function init() {
        return _init.apply(this, arguments);
      }

      return init;
    }()
  }, {
    key: "load",
    value: function () {
      var _load = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var modeledData, rawData, baseCurrency, baseFunds, symbols, symbol, openOrders, quote;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                modeledData = {};
                rawData = this._rawData;
                baseCurrency = this.seedData.tradeDetails.baseCurrency;
                baseFunds = rawData[1].balances.find(function (item) {
                  return item.asset == baseCurrency;
                });
                symbols = this.seedData.symbols;
                _context2.t0 = regeneratorRuntime.keys(symbols);

              case 6:
                if ((_context2.t1 = _context2.t0()).done) {
                  _context2.next = 25;
                  break;
                }

                symbol = _context2.t1.value;
                _context2.next = 10;
                return rawData[2][symbol];

              case 10:
                openOrders = _context2.sent;
                _context2.next = 13;
                return rawData[3][symbol];

              case 13:
                _context2.t2 = _context2.sent;
                _context2.next = 16;
                return rawData[4][symbol];

              case 16:
                _context2.t3 = _context2.sent;
                _context2.next = 19;
                return rawData[5][symbol];

              case 19:
                _context2.t4 = _context2.sent;
                quote = {
                  price: _context2.t2,
                  bidask: _context2.t3,
                  ohlc: _context2.t4
                };
                modeledData[symbol] = {
                  seedData: symbols[symbol],
                  exchangeInfo: rawData[0].symbols.find(function (item) {
                    return item.symbol == symbol;
                  }),
                  accountInfo: rawData[1].balances.find(function (item) {
                    return symbol.indexOf(item.asset) >= 0 && item.asset != baseCurrency && item.asset != "TUSD";
                  }),
                  openOrders: openOrders,
                  quote: quote,
                  baseFunds: baseFunds
                };
                modeledData[symbol].exchangeInfo.serverTime = rawData[0].serverTime;
                _context2.next = 6;
                break;

              case 25:
                this.data = modeledData;

                if (_mockDataRecorder["default"].recordData) {
                  _mockDataRecorder["default"].writeFile('hi-low-models', 'hi-low-models.json', this._rawData);
                }

              case 27:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function load() {
        return _load.apply(this, arguments);
      }

      return load;
    }()
  }]);

  return HighLowAlgoModel;
}();

module.exports = HighLowAlgoModel;