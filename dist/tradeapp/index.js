"use strict";

var _minimist = _interopRequireDefault(require("minimist"));

require("@babel/polyfill");

var _binanceApiNode = _interopRequireDefault(require("../binance-api-node"));

require("dotenv/config");

var _db = _interopRequireDefault(require("../data/db"));

var _tradeTrackerModel = _interopRequireDefault(require("../models/tradeTrackerModel"));

var _tradeLoggerModel = _interopRequireDefault(require("../models/tradeLoggerModel"));

var _algo = _interopRequireDefault(require("./algos/linearRegression/algo"));

var _model = _interopRequireDefault(require("./algos/linearRegression/model"));

var _algo2 = _interopRequireDefault(require("./algos/highLow/algo"));

var _model2 = _interopRequireDefault(require("./algos/highLow/model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var config = require('./config.json')[global.config || 'highlowStandard'];

var path = require('path');

global.appRoot = path.resolve(__dirname);
var algoMap = {
  'linear-regression': {
    algo: _algo["default"],
    model: _model["default"]
  },
  'high-low': {
    algo: _algo2["default"],
    model: _model2["default"]
  }
}; //commandline args

var args = (0, _minimist["default"])(process.argv.slice(2), {
  alias: {
    t: 'test' //run the thing live

  },
  "default": {
    t: 0
  }
}); //setup db connection

var db = new _db["default"]({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USERNAME,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DBNAME
}); //main data model

var tradeTracker = new _tradeTrackerModel["default"](db);
var logger = new _tradeLoggerModel["default"](db); //exchange connection

var exchange = (0, _binanceApiNode["default"])({
  apiKey: process.env.BINANCE_API_KEY,
  apiSecret: process.env.BINANCE_SECRET
});

if (!algoMap[config.algorithm]) {
  console.log("Algo not found");
  process.exit(1);
}

var algoModule = algoMap[config.algorithm];

var seedData = require(config.seedFile);

var model = new algoModule.model(seedData, exchange, tradeTracker, logger, config.frequency, config.orderLabel);
model.runlive = args.t ? 0 : config.live;
var algo = new algoModule.algo(model);
algo.run(); //kickit

module.exports = function () {};